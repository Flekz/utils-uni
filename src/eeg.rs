use crate::error::Result;
use std::fs;
use walkdir::WalkDir;

/// Replaces n/a n/a channels with eeg μV
pub fn fix_channels(ds_dir: String) -> Result<()> {
    for entry in WalkDir::new(ds_dir)
        .follow_links(true)
        .into_iter()
        .filter_map(|e| e.ok())
    {
        let f_name = entry.file_name().to_string_lossy();

        if f_name.ends_with("Rest_channels.tsv") {
            let path = entry.path();
            let content = fs::read_to_string(path)?;
            let new_content = content.replace("n/a\tn/a\n", "EEG\tμV\n");
            fs::write(path, new_content)?;
        }
    }
    Ok(())
}
