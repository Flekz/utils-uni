mod eeg;
mod error;
use clap::{Parser, Subcommand};
use eeg::fix_channels;
use error::Result;

#[derive(Subcommand)]
enum App {
    EegChannelFixer { database: String },
}

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    /// App
    #[command(subcommand)]
    app: App,
}

fn main() -> Result<()> {
    let cli = Cli::parse();

    match cli.app {
        App::EegChannelFixer { database: ds } => fix_channels(ds)?,
    }
    Ok(())
}
